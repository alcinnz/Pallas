# Pallas

OWL 2 EL inferencing for Vala, built on libraptor.

**NOTE** This project is currently in planning stages, but development on it should start relatively soon.

This will be used in Odysseus for reading SKOS files to use in it's bookmarking system, so it will need to understand any and all OWL statements made in the SKOS definition. Even if it's beyond the OWL 2 EL profile.

In Odysseus there will be two modes of reading RDF. The first is to traverse the RDF graph from subjects to objects from a given root URL, this would require inference of inverse properties. And the second is a simple scan of all triplets in a document, but inference on that can be more limited in favor of speed.

## Name Origins

Pallas Athena referred to the greek goddess in her affiliation to wisdom, as opposed to Nike Athena which referred to her affiliation with war and victory.